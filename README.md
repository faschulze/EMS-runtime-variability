# R E A D M E 
===========


Implementing variability using runtime arguments in the project [„Employee Payroll Management System“](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0) (originally by user Hyrex)


## Information on the open source project

The source code on this project can be found on [Gitlab](https://gitlab.com/yunocare/EMS-Antenna-Approach).

The project "EMS" is based on an open source project named *"Employee Payroll Management System"* 
published by user *"Hyrex"* (refined the project's state from: 2016-09-30). Source code on this
project can be found on [DropBox](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0) 
as well as on [Hyrex's youtube channel](youtube.com/channel/UC12Z6-QyYjcGmxgaLIxmFwg).

## Advancements

The swing-based java program was advanced with build-in variability. The user can select various configuration options to use an individual, slightly different software product. 

## Installation

The project requires at least JAVA 6 to be run.

```sh
$ clone https://gitlab.com/yunocare/EMS-runtime-variability.git
$ //cd to generated .jar file
$ //java -jar generated.jar -ARG1 -ARG2
$ 
```


Get help using:

```sh
$ //java -jar generated.jar -help
```

## Implementation with JAVA runtime arguments

The approach to implement variability is made using global runtime arguments. While using runtime arguments is a very common approach to implement software variability, we have to consider at this point, that the variability has significant impact on the applications's performance, since the "if"-conditions of global runtime arguments are checked during execution time. Also it is pretty challenging to protect unneeded code to be executed which can be a security risk. Furthermore it is challenging to trace feature in the code, therefore specific annotations can be used. In the current version of there is only naming conventing like if(FEATURENAME){..} to trace features more easily.  

This project is created for academic purpose in the context software product line development.

## Domain Analysis

The variability implementation is based on a so called [feature model](https://gitlab.com/yunocare/EMS-Antenna-Approach/blob/master/model.xml).

![feature model](./documentation/model.png)



## Project Structure

The project consists of following parts:

 - de.ovgu.isp.ems.db (contains all database related classes)
 - de.ovgu.isp.ems.core (contains core functionalities)
 - de.ovgu.isp.ems.core.logger (contains logging classes and configurations)
 - de.ovgu.isp.ems.ui (contains optional UI components) 
 - de.ovgu.isp.ems.ui.core (contains all core UI components)

**In this version there is currently no other way than using the packed SQLite database that can be found in "src/empnet.sqlite".** *(default login is "admin"/"pass")*
A Different database can be connected easily using the related JDBC connection instead of the current SQLite database connection in de.ovgu.isp.ems.db.Database.java.


## Acadamic Purpose

The project is created for academic purpose in subject of software product line implementation techniques. 



## Status

last updated: 2018-03-16
