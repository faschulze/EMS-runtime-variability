package de.ovgu.isp.ems.ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Dimension;
import java.awt.Toolkit;
import de.ovgu.isp.ems.core.Localization;
import de.ovgu.isp.ems.db.SQLiteDB;
import de.ovgu.isp.ems.db.Emp;

public class SalaryUpdater extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

	Connection conn = null;

	ResultSet rs = null;

	PreparedStatement pst = null;

	ResourceBundle messages = new Localization().getBundle();

	public static boolean updateSalaryLock = false;

	/**
	 * Creates new form updateSalary
	 */
	public SalaryUpdater() {
		initComponents();
		conn = SQLiteDB.java_db();
		Toolkit toolkit = getToolkit();
		Dimension size = toolkit.getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);

		txt_emp.setText(String.valueOf(Emp.empId).toString());
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		// TODO: put new entry to .properties
		setTitle("EMS - Salary Updater");

		// JFrame Window Configuration:
		// jframe exit listener
		WindowListener exitListener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				updateSalaryLock = false;
			}
		};
		addWindowListener(exitListener);

		// JPanel:
		jPanel4 = new JPanel();
		jPanel1 = new JPanel();

		// JLabel:
		jLabel14 = new JLabel();
		jLabel3 = new JLabel();
		jLabel2 = new JLabel();
		jLabel1 = new JLabel();
		jLabel5 = new JLabel();
		jLabel12 = new JLabel();
		jLabel9 = new JLabel();
		jLabel6 = new JLabel();
		txt_emp = new JLabel();
		jLabel10 = new JLabel();
		jLabel7 = new JLabel();
		jLabel4 = new JLabel();

		// JTextFields:
		txt_salary = new JTextField();
		txt_dept = new JTextField();
		txt_surname = new JTextField();
		txt_search = new JTextField();
		txt_empid = new JTextField();
		txt_dob = new JTextField();
		txt_firstname = new JTextField();
		txt_dept1 = new JTextField();
		txt_dept2 = new JTextField();

		// JRadioButton:
		r_amount = new JRadioButton();
		r_percentage = new JRadioButton();
		// JButton:
		jButton1 = new JButton();

		// Set Text to Labels:
		jLabel14.setText(messages.getString("employee_id") + ":");
		txt_emp.setText("emp");
		jLabel10.setText(messages.getString("logged_in_as") + " :");
		jLabel9.setText(messages.getString("department") + " :");
		jLabel3.setText(messages.getString("date_of_birth") + " :");
		jLabel2.setText(messages.getString("surname") + " :");
		jLabel1.setText(messages.getString("firstname") + " :");
		jLabel5.setText(messages.getString("employee_id") + " :");
		jLabel12.setText(messages.getString("basic_salary") + " :");
		r_percentage.setText(messages.getString("percentage") + " (%)");
		r_amount.setText(messages.getString("amount"));
		r_amount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				r_amountActionPerformed(evt);
			}
		});
		jLabel4.setText(messages.getString("update_salary_by") + " :");
		jLabel6.setText(messages.getString("percentage") + " :");
		jLabel7.setText(messages.getString("amount") + ":");

		// set non editable:
		txt_empid.setEditable(false);
		txt_dob.setEditable(false);
		txt_dept.setEditable(false);
		txt_surname.setEditable(false);
		txt_firstname.setEditable(false);
		txt_dept1.setEditable(false);
		txt_dept1.setEnabled(false);
		txt_dept2.setEditable(false);
		txt_dept2.setEnabled(false);
		txt_salary.setEditable(false);

		// Borders:
		jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, messages.getString("search"),
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Adobe Arabic", 1, 14))); // NOI18N

		// Listener:
		txt_search.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				txt_searchKeyReleased(evt);
			}
		});

		// GroupLayout
		GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(
						jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel4Layout.createSequentialGroup().addContainerGap().addComponent(jLabel14)
										.addGap(18, 18, 18).addComponent(txt_search,
												GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
										.addContainerGap()));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel4Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(txt_search, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel14))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/update icon.png"))); // NOI18N
		jButton1.setText(messages.getString("update"));
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jLabel1).addComponent(jLabel2).addComponent(jLabel5))
								.addGap(22, 22, 22)
								.addGroup(jPanel1Layout
										.createParallelGroup(GroupLayout.Alignment.LEADING, false)
										.addComponent(txt_firstname, GroupLayout.DEFAULT_SIZE, 160,
												Short.MAX_VALUE)
										.addComponent(txt_empid).addComponent(txt_surname))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(jPanel1Layout
										.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
										.addGroup(jPanel1Layout.createSequentialGroup().addComponent(jLabel9)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(txt_dept, GroupLayout.PREFERRED_SIZE, 161,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addGroup(jPanel1Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addComponent(jLabel3).addComponent(jLabel12))
												.addGap(18, 18, 18)
												.addGroup(jPanel1Layout
														.createParallelGroup(GroupLayout.Alignment.LEADING)
														.addComponent(txt_salary,
																GroupLayout.PREFERRED_SIZE, 160,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(txt_dob, GroupLayout.PREFERRED_SIZE,
																160, GroupLayout.PREFERRED_SIZE)))))
						.addGroup(jPanel1Layout.createSequentialGroup()
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jLabel4)
										.addComponent(jLabel6, GroupLayout.Alignment.TRAILING))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addGroup(jPanel1Layout.createSequentialGroup().addComponent(r_percentage)
												.addGap(18, 18, 18).addComponent(r_amount))
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addComponent(txt_dept1, GroupLayout.PREFERRED_SIZE, 95,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(jLabel7).addGap(10, 10, 10)
												.addComponent(txt_dept2, GroupLayout.PREFERRED_SIZE, 95,
														GroupLayout.PREFERRED_SIZE)
												.addGap(44, 44, 44).addComponent(jButton1,
														GroupLayout.PREFERRED_SIZE, 119,
														GroupLayout.PREFERRED_SIZE))))
						.addGroup(jPanel1Layout.createSequentialGroup().addComponent(jLabel10)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txt_emp)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(19, 19, 19).addGroup(jPanel1Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(jLabel3).addComponent(txt_dob,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(txt_salary, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(jLabel12))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(jLabel9).addComponent(txt_dept,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout
								.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jLabel5)
								.addComponent(txt_empid, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(4, 4, 4)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(jLabel1)
										.addComponent(txt_firstname, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
										.addComponent(jLabel2).addComponent(txt_surname,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))))
						.addGap(18, 18, 18)
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel4).addComponent(r_percentage).addComponent(r_amount))
						.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(18, 18, 18)
										.addGroup(jPanel1Layout
												.createParallelGroup(GroupLayout.Alignment.BASELINE)
												.addComponent(txt_dept1, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(txt_dept2, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(jLabel6).addComponent(jLabel7))
										.addGap(13, 13, 13)
										.addGroup(jPanel1Layout
												.createParallelGroup(GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel10, GroupLayout.DEFAULT_SIZE, 30,
														Short.MAX_VALUE)
												.addComponent(txt_emp)))
								.addGroup(GroupLayout.Alignment.TRAILING,
										jPanel1Layout.createSequentialGroup()
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jButton1, GroupLayout.PREFERRED_SIZE, 33,
														GroupLayout.PREFERRED_SIZE)
												.addGap(46, 46, 46)))));

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jPanel4, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		// END GroupLayout
		pack();
	}

	// </editor-fold>//GEN-END:initComponents

	/**
	 * 
	 * @param evt
	 */
	private void txt_searchKeyReleased(KeyEvent evt) {// GEN-FIRST:event_txt_searchKeyReleased
		try {

			String sql = "select * from Staff_information where id=? ";

			pst = conn.prepareStatement(sql);
			pst.setString(1, txt_search.getText());
			rs = pst.executeQuery();

			String add1 = rs.getString("id");
			txt_empid.setText(add1);

			String add2 = rs.getString("first_name");
			txt_firstname.setText(add2);

			String add3 = rs.getString("surname");
			txt_surname.setText(add3);

			String add4 = rs.getString("Dob");
			txt_dob.setText(add4);

			String add5 = rs.getString("Salary");
			txt_salary.setText(add5);

			String add8 = rs.getString("Department");
			txt_dept.setText(add8);

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {

			try {

				rs.close();
				pst.close();

			} catch (Exception e) {

			}
		}
	}

	// GEN-LAST:event_txt_searchKeyReleased

	/**
	 * 
	 * @param evt
	 */
	private void r_percentageActionPerformed(ActionEvent evt) {// GEN-FIRST:event_r_percentageActionPerformed
		r_percentage.setSelected(true);
		r_amount.setSelected(false);
		// r_amount.setEnabled(false);
		txt_dept2.setEnabled(false);
		txt_dept1.setEditable(true);
		txt_dept1.setEnabled(true);
		txt_dept2.setText("");

	}

	// GEN-LAST:event_r_percentageActionPerformed

	/**
	 * 
	 * @param evt
	 */
	private void r_amountActionPerformed(ActionEvent evt) {// GEN-FIRST:event_r_amountActionPerformed
		r_amount.setSelected(true);
		r_percentage.setSelected(false);
		// r_percentage.setEnabled(false);
		txt_dept1.setEnabled(false);
		txt_dept2.setEditable(true);
		txt_dept2.setEnabled(true);
		txt_dept1.setText("");
	}

	// GEN-LAST:event_r_amountActionPerformed

	/**
	 * 
	 * @param evt
	 */
	private void jButton1ActionPerformed(ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		int p = JOptionPane.showConfirmDialog(null, messages.getString("confirm_update_salary"),
				messages.getString("update_record"), JOptionPane.YES_NO_OPTION);
		if (p == 0) {
			int salary = Integer.parseInt(txt_salary.getText());

			if (r_percentage.isSelected() == true) {
				int getPercentage = Integer.parseInt(txt_dept1.getText());
				int calcPercentage = salary / 100 * getPercentage + salary;
				String xP = String.valueOf(calcPercentage);
				txt_salary.setText(xP);
			}

			else if (r_amount.isSelected() == true) {
				int getAmt = Integer.parseInt(txt_dept2.getText());
				int calcAmount = salary + getAmt;
				String xA = String.valueOf(calcAmount);
				txt_salary.setText(xA);
			}
			// INSERT into AUDIT
			try {
				Localization _loc = new Localization();
				String val = txt_emp.getText().toString();
				String reg = "insert into Audit (emp_id, date, status) values ('" + val + "','" + _loc.getTimeString()
						+ " / " + _loc.getDateString() + "','Updated Salary Record')";
				pst = conn.prepareStatement(reg);
				pst.execute();
			} catch (Exception e)

			{
				JOptionPane.showMessageDialog(null, e);
			}
			// UPDATE table STAFF_INFORMATION:
			try {

				// Preparing to update staff salary
				String value1 = txt_empid.getText();
				String value2 = txt_salary.getText();
				// update staff salary by amount:
				String sql = "update Staff_information set id='" + value1 + "',Salary='" + value2 + "' where id='"
						+ value1 + "'";

				pst = conn.prepareStatement(sql);
				// execute sql
				pst.execute();
				JOptionPane.showMessageDialog(null, messages.getString("record_updated"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);
			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {

				}
			}
		}

	}

	// GEN-LAST:event_jButton1ActionPerformed

	/**
	 * @param args
	 */
	public static void main(String args[]) {

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new SalaryUpdater().setVisible(true);
			}
		});
	}

	private JButton jButton1;
	private JLabel jLabel1;
	private JLabel jLabel10;
	private JLabel jLabel12;
	private JLabel jLabel14;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel9;
	private JPanel jPanel1;
	private JPanel jPanel4;
	private JRadioButton r_amount;
	private JRadioButton r_percentage;
	private JTextField txt_dept;
	private JTextField txt_dept1;
	private JTextField txt_dept2;
	private JTextField txt_dob;
	private JLabel txt_emp;
	private JTextField txt_empid;
	private JTextField txt_firstname;
	private JTextField txt_salary;
	private JTextField txt_search;
	private JTextField txt_surname;
}
